Este parche a�ade correcciones al lenguaje espa�ol de la versi�n original.
Asimismo, mejora el soporte en ingl�s de la misma versi�n.
Para instalarlo, ejecuta como root en una consola "sh install.sh" y el
parche ser� aplicado a la versi�n de pycoder previamente instalada.
Solo funciona con pycoder 0.1.1


Traducci�n al ingl�s Austin Acton <aacton@yorku.ca>

Ear3ndil <ear@emulinux.net>