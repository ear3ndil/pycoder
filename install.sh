#!/bin/sh

#Instalador simple de Vcd PyCoder
#Por Ear3ndil. http://acidnet.dyndns.org/vcd

chmod +x vcd.py
chmod +x pycoder

mv /usr/share/pycoder /usr/share/pycoder.old
mkdir /usr/share/pycoder
cp vcd.py lang* /usr/share/pycoder
cp pycoder /usr/local/bin

echo "The installation is finished."
