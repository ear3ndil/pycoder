#!/usr/bin/python



textl1="""\nVCD PyCoder 0.1.1\nConversor de video DivX a VCD. Esta versi�n est� en desarrollo.\n\n\nVisitad nuestra web en http://acidnet.dyndns.org/vcd\n\n\n\nPor Ear3ndil <ear@emulinux.net>"""
textl2="\n\nVCD PyCoder 0.1.1\nConversor de video DivX a VCD\n\n"
textl3="Caracter�sticas del video"
yesno1="si"
yesno2="no"
#Menus
menul1="&Abrir video"
menul11="Abrir y ver caracter�sticas de una pel�cula"
menul3="&Grabar a CD"
menul33="Grabar una pel�cula en formato VCD a CD"
menul4="&Salir"
menul44="Cierra el programa"
menul5="&Ver video"
menul55="Ver una pel�cula en diversos formatos"
menul6="Ver &VCD"
menul66="Ver una pel�cula en VCD desde cdrom"
menul7="Preferencias generales"
menul77="Opciones y configuracion de parametros de uso general"
menul7a="&Idioma"
menul77a="Elige tu idioma preferido"
menul8="Ay&uda"
menul88="Ayuda sobre este programa"
menul9="&Acerca"
menul99="Acerca de este programa"
menutools0="Mpg videos"
menutools0a="Herramientas de tratamiento de videos mpg"
menutools1="&Caracter�sticas"
menutools2="C&ortar video"
menutools3="&Pegar video"
menutools4="Avi videos"
menutools4a="Herramientas de tratamiento de videos avi"
barral1="&Archivo"
barral2="&Ver"
barral3="&Opciones"
barral4="&Acerca"
barral5="&Editar"
acercal1="VCD PyCoder, creaci�n de VCD desde formatos de video DivX.\n Por Ear3ndil, 2003. <ear@emulinux.net>"
acercal2="Acerca de VCD PyCoder 0.1.1"
abrirl1="Elige un archivo"
abrirl2="Caracter�sticas del archivo"
abrirl3="Crear video"
abrirl33="Creaci�n de un video completo"
abrirl3a="Crear ejemplo"
abrirl3b="Creaci�n de un video de ejemplo de corta duraci�n"
abrirl4="Volver"
abrirl44="Volver al menu principal"
abrirl5="Selecciona el modo de conversi�n"
convertl1="Elige un archivo"
convertl2="&Continuar"
convertl3="Convertir una pelicula DivX a VCD"
convertl4="&Volver"
convertl5="Volver al menu principal"
convertl6="&Opciones"
convertl7="Selecciona el modo de video:"
convertl8="Selecciona la calidad de audio:"
convertl9="Selecciona el tama�o del CD:"
convertl11="�C�lculo de los bordes?"
convertl11a="Peque�o"
convertl11b="Mediano"
convertl11c="Completo"
convertl12="-Opciones de conversi�n-"
codel22="La codificaci�n va a empezar.\nEste proceso durar� varias horas durante las cuales el programa queda en suspendido.\nEn la pr�xima version podras ver el proceso de codificacion. En esta no toca, lo siento:P)"
codel33="Codificar video ..."
codel1="\nLa pelicula se est� codificando. Este proceso puede durar varias horas.\n Mientras tanto, Vcd PyCoder queda en suspendido hasta que la codificaci�n finalice."
codel2="La codificaci�n ha terminado. Revisa la calidad de la codificaci�n antes de grabar la pelicula."
codel3="Codificar video ..."
grabarl1="Elige un archivo para grabar"
grabarl11="Se van a crear los archivos cue en formato VCD.\nEspera durante unos minutos hasta que se creen..."
grabarl2="\nCreando los archivos cue con vcdimager..."
grabarl3="\nArchivos cue creados. Preparado para grabar..."
grabarl4="Introduce un CD virgen en la grabadora.\nRecuerda, puedes necesitar ser root o tener los permisos adecuados para poder grabar."
grabarl5="Grabar CD..."
grabarl6="\nGrabando la pel�cula a formato vcd con cdrdao"
grabarl7="La grabaci�n ha concluido"
grabarl8="Grabar CD..."
grabarl9="Modo de video de grabaci�n"
grabarl1a="Elige el modo de video de grabaci�n"
vcdl1="Introduce el VCD en el cdrom y pulsa OK"
vcdl2="Ver un VCD..."
vcdl3="\nFin de la reproducci�n"
verl1="Elige un archivo"
verl2="\nReproduciendo..."
verl3="\nFin de la reproducci�n..."
ayudal1="Abriendo el navegador para ver la ayuda"
preferl1="&Guardar"
preferl2="Guardar las opciones"
preferl3="&Salir"
preferl4="Volver al menu principal"
preferl5="&Opciones"
preferl6="Mplayer video"
preferl7="Opciones de video Mplayer"
preferl8="Navegador por defecto"
preferl9="Dispositivo SCSI para cdrdao"

#A�adidas en versi�n 0.1.1

samplel1="Crear video"
samplel2="Volver a crear el video completo"
explicavcd="El VCD es un formato est�ndar de video a una resoluci�n fija de 352x288 (PAL) o\n352x240 (NTSC). El c�lculo del tama�o de un VCD es equivalente al tama�o del CD, \nen un CD de 80 minutos caben 80 minutos de video VCD. Si la pel�cula dura 160 minutos\nnecesitaremos dos CDs de 80 minutos. Por tanto, se suele necesitar mas de un CD para\ngrabar una pel�cula.\n\nEste formato de video es est�ndar y la mayoria de los DVD deberian poder reproducirlo\nsin ning�n tipo de  problema."
explicasvcd="El formato SVCD o Super VCD es un formato de video a una resoluci�n de 480x576 (PAL) o\n480x480 (NTSC). La calidad de video es m�s alta que bajo formato VCD pero\ntambi�n ocupa m�s tama�o, soliendo estar entre 40 y 50 minutos de video en un CD de\n80 minutos. Seguramente necesitaremos unos tres o cuatro CDs para grabar un video\nde unas dos horas.\n\nEste formato suele ser reproducible por la mayoria de los DVD aunque es un poco menos\nfrecuente que el formato VCD."
explicacvcd="El CVCD es una variaci�n del estandar VCD. B�sicamente consiste en ajustar el bitrate\n del video para reducir el tama�o y que quepa en un �nico CD del tama�o que elijamos.\nA m�s tama�o, mejor calidad.\n\nEste formato puede presentar incompatibilidades con los DVD m�s antiguos. Si presenta\nproblemas, utiliza el modo VCD est�ndar."
convertlsample1="Selecciona la duraci�n del ejemplo:"
