#!/usr/bin/python

#Vcd PyCoder 0.1.1
#Jordi Ivars (Ear3ndil). 23 Abril 2003

#Importamos los modulos necesarios
import string  #Trabajo con cadenas
import os      #Ejecutar comandos de sistema
from wxPython.wx import *    #Modulos gr�ficos
import pickle	#Lo usamos para importar contenidos de archivos


#Creamos los IDS para cada def, asignandoles un numero no repetido e
#identificatorio. Dos IDS con el mismo numero son el mismo.

ID_HOMEC=1
ID_HOMEP=2
ID_ACERCA=101
ID_SALIR=110
ID_ABRIR=102
ID_PREFS=103
ID_CONVERT=104
ID_AYUDA=105
ID_VER=106
ID_VCD=109
ID_MPLAYER=107
ID_CODE=108
ID_GRABAR=115
ID_GUARDAR=116
ID_LANG=117
ID_LANGES=118
ID_LANGENG=119
ID_CODE=120
ID_HOME=121
ID_MPGCORTAR=122
ID_MPGPEGAR=123
ID_MPGINFO=127
ID_AVIPEGAR=124
ID_AVICORTAR=125
ID_AVIINFO=128
ID_TOOLS=126
ID_SAMPLE=130
ID_SAMPLEMENU=131

#Creamos la ventana principal MainWindow, con el marco principal,
#dimensiones (size), estilo (style). Se crean tambien la barra de estado y
#demas.


class Ventana(wxFrame):
	def __init__(self,parent,id,title):

		wxFrame.__init__(self,parent,-4,title,size=(560,340),style=wxDEFAULT_FRAME_STYLE|wxNO_FULL_REPAINT_ON_RESIZE)

#Abrimos el archivo de configuraci�n del usuario

		self.lee_config()

		def importa(arch):
			import imp
			d=imp.find_module(arch)
			return imp.load_module(arch,*d)


#Importamos el modulo lang, definido en la anterior funcion y a traves del archivo de config

		self.lang=importa(self.config["lang"])

		self.control=wxStaticText(self, -1, self.lang.textl1, (20,100),style=wxALIGN_CENTER)


#Establecemos valores de creaci�n de video por defecto

		self.transcode = {'vmode':'vcd', 'video': 'pal','audio' : '128','cdsize' : '650','border' : self.lang.convertl11a ,'calidad':'5','sampletime':'5 min'}

#Creamos las fuentes

		self.font = wxFont(9.5, wxROMAN, wxNORMAL, wxNORMAL) #Negrita wxBOLD

#Aplicamos las fuentes a la pantalla inicial

		self.control.SetFont(self.font)

#Aplicamos las fuentes a toda la aplicacion

		self.SetFont(self.font)
		#self.control.SetSize(wxSize(200, 100))
		self.CreateStatusBar()

#Definimos los menus. A cada uno de los menus le a�adismos sus opciones.

		menu1=wxMenu()
		menu1.Append(ID_ABRIR,self.lang.menul1,self.lang.menul11)
		menu1.Append(ID_GRABAR,self.lang.menul3,self.lang.menul33)
		menu1.AppendSeparator()
		menu1.Append(ID_SALIR,self.lang.menul4,self.lang.menul44)

		menu4=wxMenu()
		menu4.Append(ID_VER,self.lang.menul5,self.lang.menul55)
		menu4.Append(ID_VCD,self.lang.menul6,self.lang.menul66)

		menu5=wxMenu()
		submenumpg=wxMenu()
		submenuavi=wxMenu()
		submenumpg.Append(ID_MPGINFO,self.lang.menutools1)
		#submenumpg.Append(ID_MPGCORTAR,self.lang.menutools2)
		#submenumpg.Append(ID_MPGPEGAR,self.lang.menutools3)
		menu5.AppendMenu(ID_TOOLS,self.lang.menutools0,submenumpg,self.lang.menutools0a)
		submenuavi.Append(ID_AVIINFO,self.lang.menutools1)
		#submenuavi.Append(ID_AVICORTAR,self.lang.menutools2)
		#submenuavi.Append(ID_AVIPEGAR,self.lang.menutools3)
		menu5.AppendMenu(ID_TOOLS,self.lang.menutools4,submenuavi,self.lang.menutools4a)

		menu2=wxMenu()
		menu2.Append(ID_PREFS,self.lang.menul7,self.lang.menul77)

		submenulang = wxMenu()
		submenulang.Append(ID_LANGES,"Espa�ol")
		submenulang.Append(ID_LANGENG,"English")

		menu2.AppendMenu(ID_LANG,self.lang.menul7a,submenulang,self.lang.menul77a)

		menu3=wxMenu()
		menu3.Append(ID_AYUDA, self.lang.menul8, self.lang.menul88)
		menu3.Append(ID_ACERCA,self.lang.menul9, self.lang.menul99)

#Creamos los menus llamando a lo definido anteriormente.

		Barra=wxMenuBar()
		Barra.Append(menu1,self.lang.barral1)
		Barra.Append(menu5,self.lang.barral5)
		Barra.Append(menu4,self.lang.barral2)
		Barra.Append(menu2,self.lang.barral3)
		Barra.Append(menu3,self.lang.barral4)

		self.SetMenuBar(Barra)

#Asociamos los IDS anteriormente definidos con un tipo de evento (EVT_MENU)
#y una funcion (self.Algo)

		EVT_MENU(self,ID_ABRIR,self.Abrir)

		EVT_MENU(self,ID_ACERCA,self.Acerca)

		EVT_MENU(self,ID_SALIR,self.Salir)

		EVT_MENU(self,ID_VER,self.Ver)

		EVT_MENU(self,ID_VCD,self.Vcd)

		EVT_MENU(self,ID_AYUDA,self.Ayuda)

		EVT_MENU(self,ID_GRABAR,self.Grabar)

		EVT_MENU(self,ID_CONVERT,self.Convert)

		EVT_MENU(self,ID_PREFS,self.Prefer)

		EVT_MENU(self,ID_HOMEC,self.Homec)

		EVT_MENU(self,ID_HOMEP,self.Homep)

		EVT_MENU(self,ID_HOME,self.Home)

		EVT_MENU(self,ID_LANGES,self.Langes)

		EVT_MENU(self,ID_LANGENG,self.Langeng)

		EVT_MENU(self,ID_CODE,self.Code)

		EVT_MENU(self,ID_MPGCORTAR,self.MpgCortar)

		EVT_MENU(self,ID_MPGINFO,self.MpgInfo)

		EVT_MENU(self,ID_AVIINFO,self.AviInfo)

		EVT_MENU(self,ID_SAMPLE,self.Sample)

		EVT_MENU(self,ID_SAMPLEMENU,self.SampleMenu)

		self.Show(true)

	def lee_config(self):

		directorio=wxGetHomeDir()

		archivo=".pycoder"

		todo=os.path.join(directorio,archivo)

		self.config = {}

		try:
			f=open(todo, "r")
			self.config = pickle.load(f)

		except:

			self.config = {'navegador': 'netscape','vo' : 'x11','cdrdaodev':'0,0,0','lang':'lang_es'}
			self.save_config()

	def save_config(self):

		directorio=wxGetHomeDir()

		archivo=".pycoder"

		todo=os.path.join(directorio,archivo)

		try:
			f=open(todo, "w")
			pickle.dump(self.config, f)

			return 1
		except:
			return 0

#Empezamos a crear las distintas funciones pertenecientes a los distintos
#menus.

	def Acerca(self,e):

#Creacion de una ventana con mensaje determinado. Definimos al final el tipo
#de salida que tiene la ventana wxOK y el icono, wxICON*. Despues la
#mostramos con ShowModal

		d=wxMessageDialog(self,self.lang.acercal1, self.lang.acercal2,wxOK|wxICON_INFORMATION)
		d.ShowModal()
		d.Destroy()

	def Salir(self,e):

		self.Close(true)


	def Abrir(self,e):

		self.dirname=wxGetHomeDir()
		#self.transcode = {'vmode':'vcd', 'video': 'pal','audio' : '128','cdsize' : '650','border' : self.lang.convertl11a ,'calidad':'5','sampletime':'5 min'}

		aabrir=wxFileDialog(self,self.lang.abrirl1,self.dirname,"","*.avi",wxOPEN)

		if aabrir.ShowModal() == wxID_OK:

			menu0=wxMenu()
			menu0.Append(ID_CONVERT,self.lang.abrirl3, self.lang.abrirl33)
			menu0.Append(ID_SAMPLEMENU,self.lang.abrirl3a,self.lang.abrirl3b)
			menu0.Append(ID_HOME,self.lang.abrirl4,self.lang.abrirl44)

			BarraX=wxMenuBar()
			BarraX.Append(menu0,self.lang.convertl6)
			self.SetMenuBar(BarraX)

			self.filename=aabrir.GetFilename()
			self.dirname=aabrir.GetDirectory()
			self.pelicula=os.path.join(self.dirname,self.filename)
			#os.system("""echo "\n" """+" "+self.lang.abrirl2+" "+`self.filename`+"  >/tmp/pycoder")
			#os.system("""echo "\n" > /tmp/pycoder""")
			os.system('tcprobe -i'+" "+`self.pelicula` +" >/tmp/pycoder")

			#babrir=open(os.path.join('/tmp/pycoder'),'r')
			self.control.Destroy()
			#self.control=wxStaticText(self, -1, babrir.read(), (0,0),style=wxALIGN_LEFT)
			#babrir.close()

			videolista = ['vcd', 'svcd', 'cvcd']

			self.vmode=wxRadioBox(self, 41, self.lang.abrirl5, wxPoint(10,10),wxSize(250,50),videolista, 3, wxRA_SPECIFY_COLS)
			EVT_RADIOBOX(self, 41, self.VideoMode)

			babrir=open(os.path.join('/tmp/pycoder'),'r')
			tcprobez=babrir.read()
			j=wxMessageDialog(self,tcprobez,self.lang.textl3,wxOK)
			j.Show()
			babrir.close()

			self.vmode2=wxStaticText(self,-1,self.lang.explicavcd,(50,100),style=wxALIGN_LEFT)

#			class LogWindow(wxTextCtrl):
	#			def __init__ (self):

#					frame2 = wxFrame(None,-10, "Caracter�sticas...", size=wxSize(570,300))
	#				frame2.Show(true)
		#			self.parent = frame2
			#		wxTextCtrl.__init__(self,self.parent,-1,size = self.parent.GetClientSize(),style=wxTE_MULTILINE|wxTE_READONLY|wxHSCROLL)

		#wxStaticText(self, -10, "lalA", (0,0),style=wxALIGN_LEFT)
		#frame2.__init__self,self.parent,-10,size = self.parent.GetClientSize(),style=wxTE_MULTILINE|wxTE_READONLY|wxHSCROLL)

#					sys.stdout = self
	#				babrir=open(os.path.join('/tmp/pycoder'),'r')
		#			z=babrir.read()
			#		print z
				#	babrir.close()


#		frame = Ventana2(None,-2,"lala")


#			class MainApp(wxApp):
	#			def OnInit(self):

#					log = LogWindow()
	#				log.parent.Show(true)

#					self.SetTopWindow(log.parent)
	#				return true


#			app2 = MainApp(0)
	#		app2.MainLoop()

#			app2.Destroy()

		aabrir.Destroy()

	def Convert(self,e):

		menu0=wxMenu()
		menu0.Append(ID_CODE, self.lang.convertl2, self.lang.convertl3)
		menu0.Append(ID_HOMEC,self.lang.convertl4,self.lang.convertl5)

		BarraX=wxMenuBar()
		BarraX.Append(menu0,self.lang.convertl6)
		self.SetMenuBar(BarraX)

		self.vmode.Destroy()
		self.vmode2.Destroy()

		#self.control.Destroy()

		self.convert0=wxStaticText(self,-1,self.lang.convertl12,wxPoint(40,30),wxSize(300,30))

		lista=['pal','ntsc']
		self.convert1 = wxStaticText(self, -1, self.lang.convertl7, wxPoint(20,70), wxSize(275, 50))
		self.convert2 = wxChoice(self, 40, (80, 100), choices = lista)

		EVT_CHOICE(self,40,self.ConvertVideoMode)


		lista2=['128','224']
		self.convert3 = wxStaticText(self,-1,self.lang.convertl8,wxPoint(20,150), wxSize(275,50))
		self.convert4 = wxChoice(self,41,(80,170),choices = lista2)


		EVT_CHOICE(self, 41, self.ConvertAudioMode)

		lista3=['650','700','800','900']

		self.convert5 = wxStaticText(self,-1,self.lang.convertl9,wxPoint(260,70), wxSize(275,50))
		self.convert6 = wxChoice(self,42,(300,100),choices = lista3)

		EVT_CHOICE(self,42,self.ConvertCdSize)

		lista4=[self.lang.convertl11a,self.lang.convertl11b,self.lang.convertl11c]
		self.convert7 = wxStaticText(self, -1, self.lang.convertl11,wxPoint(260,150),wxSize(275,50))
		self.convert8=wxChoice(self,43,(300,170),choices = lista4)

		EVT_CHOICE(self,43,self.ConvertBorder)


	def Code(self,e):

		self.convert0.Destroy()
		self.convert3.Destroy()
		self.convert1.Destroy()
		self.convert2.Destroy()
		self.convert4.Destroy()
		self.convert5.Destroy()
		self.convert6.Destroy()
		self.convert7.Destroy()
		self.convert8.Destroy()

		menu1=wxMenu()
		menu1.Append(ID_ABRIR,self.lang.menul1,self.lang.menul11)
		menu1.Append(ID_GRABAR,self.lang.menul3,self.lang.menul33)
		menu1.AppendSeparator()
		menu1.Append(ID_SALIR,self.lang.menul4,self.lang.menul44)

		menu4=wxMenu()
		menu4.Append(ID_VER,self.lang.menul5,self.lang.menul55)
		menu4.Append(ID_VCD,self.lang.menul6,self.lang.menul66)

		menu2=wxMenu()
		menu2.Append(ID_PREFS,self.lang.menul7,self.lang.menul77)

		submenulang = wxMenu()
		submenulang.Append(ID_LANGES,"Espa�ol")
		submenulang.Append(ID_LANGENG,"English")

		menu2.AppendMenu(ID_LANG,self.lang.menul7a,submenulang,self.lang.menul77a)

		menu3=wxMenu()
		menu3.Append(ID_AYUDA, self.lang.menul8, self.lang.menul88)
		menu3.Append(ID_ACERCA,self.lang.menul9, self.lang.menul99)

#Creamos los menus llamando a lo definido anteriormente.

		Barra=wxMenuBar()
		Barra.Append(menu1,self.lang.barral1)
		Barra.Append(menu4,self.lang.barral2)
		Barra.Append(menu2,self.lang.barral3)
		Barra.Append(menu3,self.lang.barral4)

		self.SetMenuBar(Barra)

		self.control=wxStaticText(self, -1, self.lang.codel1, (0,0),style=wxALIGN_CENTER)

###
#VCD
###
		if self.transcode["vmode"]=="vcd":

			if self.transcode["video"] == "pal":

				fps=25
				width="352x288"
				width_out=352
				height_out=288.0
			#avifix -i self.pelicula -f 25
			#os.system("avifix -i"+" "+ self.pelicula+ " "+ "-f 25")

			else:
				#ntsc
				fps=29
				width="352x240"
				width_out=352
				height_out=240.0
			#avifix -i self.pelicula -f 29
			#os.system("avifix -i"+" "+self.pelicula+ " "+ "-f 29")


#Sacamos los valores originales de la pelicula, volcandolos a un tmp.
#El calculo de los bordes est� sacado de los scripts de paranouei en http://dvdripping-guid.berlios.de/Divx-to-VCD.html
#El calculo de bordes automatico resulta inestable. Con resoluciones en origen muy altas da como resultado imagenes mezcladas.
#Damos opcion de no usarlo

			os.system("grep '\[avilib\] V:' /tmp/pycoder | perl -e ' $line=<STDIN> ; $line =~ /width=(\d+)/; print $1' > /tmp/vcd_width")

			os.system("grep '\[avilib\] V:' /tmp/pycoder | perl -e ' $line=<STDIN> ; $line =~ /height=(\d+)/ ; print $1' > /tmp/vcd_height")


#Abrimos los archivos creados anteriormente con los valores que necesitamos

			acode=open(os.path.join('/tmp/vcd_height'),'r')
			vcd_height=acode.read()
			acode.close()

			bcode=open(('/tmp/vcd_width'),'r')
			vcd_width=bcode.read()
			bcode.close()

#Realizamos el calculo de los bordes

			alturab =  height_out * 4 / 3

			altura = int(vcd_height) / (int(vcd_width) / alturab)


#Redondeo para asegurar que el numero sea par. Si es impar transcode dar� error

			altura=(int(altura))+int(altura)%2

			borde = (int(height_out)-altura)/2


#Eleccion de si queremos borde o no. A la larga habra que implementar una calculadora de bordes. De momento, aplicamos la posibilidad de cambiarle el aspect ratio para reducir los bordes, aunque
#no lo solcuona del todo. Hay que depurar el c�lculo de bordes original de paranouei para que funcione con todos los videos.


			#borde calculado

			if self.transcode["border"]==self.lang.convertl11c:
				borde=borde
				aspect=2

			#borde mediano sacado con aspect 3

			elif self.transcode["border"]==self.lang.convertl11b:
				borde=0
				altura=int(height_out)
				aspect=3

			#Sin borde
			elif self.transcode["border"]==self.lang.convertl11a:
				borde=0
				altura=int(height_out)
				aspect=2


#Sacamos el numero de frames de la pelicula para poder calcular su duraci�n en segundos

			os.system("grep '\[avilib\] V:' /tmp/pycoder | perl -e ' $line=<STDIN> ; $line=~ /frames=(\d+)/  ;  print $1'>/tmp/frames")

			dcode=open(('/tmp/frames'),'r')
			vcd_frames=dcode.read()
			dcode.close()

#Calculamos la duracion de la pelicula, primero en segundos y luego en minutos

			totaltime=(int(vcd_frames)/fps)
			totalminuts=totaltime/60

			dconvert=wxMessageDialog(self,self.lang.codel22, self.lang.codel33, wxOK|wxICON_EXCLAMATION)
			dconvert.ShowModal()
			dconvert.Destroy()

			wxEXEC_SYNC=1
			wxExecute('transcode -i %s -V -x mplayer -y mpeg2enc,mp2enc -F 1 -Z %sx%s -Y -%s,0,-%s,0 --export_asr %s -E 44100 -w 1150 -R 3 -b %s -o film' % (self.pelicula,width_out,altura,borde,borde,aspect,self.transcode["audio"]),wxEXEC_SYNC)


#Dependiendo de la calidad de audio se usa un tama�o u otro. Tcmplex calcula la duracion por el video y no por el audio. As� que si le das un valor de 4200 segundos es ese valor mas el del audio, por lo que hay que calcular unos valores para audio 128 y otro para 224.


			if self.transcode["cdsize"]=="650":

		#Calculamos el n�mero total de cds que necesitaremos y redondeamos para arriba (+1)

				cds=(totalminuts/74)+1

				cdsegons1=0
				cdsegons2=3910    #Esto es lo m�ximo que cabe en un cd
				cdsegons3=3910

				a=0

		#Establecemos que se graben tantos cds por numero de cds que se hayan calculado

				while a < cds:

					a=a+1

					wxExecute('tcmplex -c %s-%s -m 1 -i film.m1v -p film.mpa -o film-cd%s.mpg' % (cdsegons1,cdsegons2,a),wxEXEC_SYNC)

		#Doblamos el tama�o de los segundos a medida que subimos en el n�mero de cds

					cdsegons1=cdsegons2
					cdsegons2=cdsegons2+cdsegons3

			elif self.transcode["cdsize"]=="700":

				cds=(totalminuts/80)+1

				cdsegons1=0
				cdsegons2=4200
				cdsegons3=4200

				a=0

				while a < cds:

					a=a+1

					wxExecute('tcmplex -c %s-%s -m 1 -i film.m1v -p film.mpa -o film-cd%s.mpg' % (cdsegons1,cdsegons2,a),wxEXEC_SYNC)

					cdsegons1=cdsegons2
					cdsegons2=cdsegons2+cdsegons3

			elif self.transcode["cdsize"]=="800":

				cds=(totalminuts/90)+1

				cdsegons1=0
				cdsegons2=4815
				cdsegons3=4815

				a=0

				while a < cds:

					a=a+1

					wxExecute('tcmplex -c %s-%s -m 1 -i film.m1v -p film.mpa -o film-cd%s.mpg' % (cdsegons1,cdsegons2,a),wxEXEC_SYNC)

					cdsegons1=cdsegons2
					cdsegons2=cdsegons2+cdsegons3

			elif self.transcode["cdsize"]=="900":

				cds=(totalminuts/100)+1

				cdsegons1=0
				cdsegons2=5410
				cdsegons3=5410

				a=0

				while a < cds:

					a=a+1

					wxExecute('tcmplex -c %s-%s -m 1 -i film.m1v -p film.mpa -o film-cd%s.mpg' % (cdsegons1,cdsegons2,a),wxEXEC_SYNC)

					cdsegons1=cdsegons2
					cdsegons2=cdsegons2+cdsegons3

###
#SVCD
###

		elif self.transcode["vmode"]=="svcd":

			calidad="7"
			maxvbr="4000000"

 			if self.transcode["video"] == "pal":

				fps=25
				width="480x576"
				width_out=480
				height_out=576.0
				os.system("echo quant_value = %s > /tmp/template" % (calidad))
				os.system("echo max_bitrate = %s >> /tmp/template" % (maxvbr))
				mpeg_options="s,3,/tmp/template"

			else:
				#ntsc
				fps=29
				width="480x480"
				width_out=480
				height_out=480.0
				os.system("echo quant_value = %s > /tmp/template" % (calidad))
				os.system("echo max_bitrate = %s >> /tmp/template" % (maxvbr))
				mpeg_options="s,2,/tmp/template"


			os.system("grep '\[avilib\] V:' /tmp/pycoder | perl -e ' $line=<STDIN> ; $line =~ /width=(\d+)/; print $1' > /tmp/vcd_width")

			os.system("grep '\[avilib\] V:' /tmp/pycoder | perl -e ' $line=<STDIN> ; $line =~ /height=(\d+)/ ; print $1' > /tmp/vcd_height")


			acode=open(os.path.join('/tmp/vcd_height'),'r')
			vcd_height=acode.read()
			acode.close()

			bcode=open(('/tmp/vcd_width'),'r')
			vcd_width=bcode.read()
			bcode.close()

			alturab =  height_out * 4 / 3

			altura = int(vcd_height) / (int(vcd_width) / alturab)


			altura=(int(altura))+int(altura)%2

			borde = (int(height_out)-altura)/2

			if self.transcode["border"]==self.lang.convertl11c:
				borde=borde
				aspect=2

			elif self.transcode["border"]==self.lang.convertl11b:
				borde=0
				altura=int(height_out)
				aspect=3

			elif self.transcode["border"]==self.lang.convertl11a:
				borde=0
				altura=int(height_out)
				aspect=2

#Asignaci�n de bordes antiguo
#			if self.transcode["border"]==self.lang.yesno1:
#				borde=borde
#			else:
#				borde=0
#				altura=int(height_out)

			os.system("grep '\[avilib\] V:' /tmp/pycoder | perl -e ' $line=<STDIN> ; $line=~ /frames=(\d+)/  ;  print $1'>/tmp/frames")

			dcode=open(('/tmp/frames'),'r')
			vcd_frames=dcode.read()
			dcode.close()

			totaltime=(int(vcd_frames)/fps)
			totalminuts=totaltime/60

			dconvert=wxMessageDialog(self,self.lang.codel22, self.lang.codel33, wxOK|wxICON_EXCLAMATION)
			dconvert.ShowModal()
			dconvert.Destroy()

			wxEXEC_SYNC=1
			wxExecute('transcode -i %s -V -k -x mplayer -y mpeg -F %s -Z %sx%s -Y -%s,0,-%s,0  --export_asr %s -R 3 -E 44100 -b %s -o film' % (self.pelicula,mpeg_options,width_out,altura,borde,borde,aspect,self.transcode["audio"]),wxEXEC_SYNC)

			if self.transcode["cdsize"]=="650":

				cds=(totalminuts/74)+1

				cdsegons1=0
				cdsegons2=3910
				cdsegons3=3910

				a=0

				while a < cds:

					a=a+1

					wxExecute('tcmplex -c %s-%s -m s -i film.m2v -p film.mpa -o film-cd%s.mpg' % (cdsegons1,cdsegons2,a),wxEXEC_SYNC)

					cdsegons1=cdsegons2
					cdsegons2=cdsegons2+cdsegons3

			elif self.transcode["cdsize"]=="700":

				cds=(totalminuts/80)+1

				cdsegons1=0
				cdsegons2=4200
				cdsegons3=4200

				a=0

				while a < cds:

					a=a+1

					wxExecute('tcmplex -c %s-%s -m s -i film.m2v -p film.mpa -o film-cd%s.mpg' % (cdsegons1,cdsegons2,a),wxEXEC_SYNC)

					cdsegons1=cdsegons2
					cdsegons2=cdsegons2+cdsegons3

			elif self.transcode["cdsize"]=="800":

				cds=(totalminuts/90)+1

				cdsegons1=0
				cdsegons2=4815
				cdsegons3=4815

				a=0

				while a < cds:

					a=a+1

					wxExecute('tcmplex -c %s-%s -m s -i film.m2v -p film.mpa -o film-cd%s.mpg' % (cdsegons1,cdsegons2,a),wxEXEC_SYNC)

					cdsegons1=cdsegons2
					cdsegons2=cdsegons2+cdsegons3

			elif self.transcode["cdsize"]=="900":

				cds=(totalminuts/100)+1

				cdsegons1=0
				cdsegons2=5410
				cdsegons3=5410

				a=0

				while a < cds:

					a=a+1

					wxExecute('tcmplex -c %s-%s -m s -i film.m2v -p film.mpa -o film-cd%s.mpg' % (cdsegons1,cdsegons2,a),wxEXEC_SYNC)

					cdsegons1=cdsegons2
					cdsegons2=cdsegons2+cdsegons3

###
#CVCD
###

		elif self.transcode["vmode"]=="cvcd":

			if self.transcode["video"] == "pal":

				#fps=
				width="352x288"
				width_out=352
				height_out=288.0
			else:
				#ntsc
				#fps=23.976
				width="352x240"
				width_out=352
				height_out=240.0


			os.system("grep '\[avilib\] V:' /tmp/pycoder | perl -e ' $line=<STDIN> ; $line =~ /width=(\d+)/; print $1' > /tmp/vcd_width")

			os.system("grep '\[avilib\] V:' /tmp/pycoder | perl -e ' $line=<STDIN> ; $line =~ /height=(\d+)/ ; print $1' > /tmp/vcd_height")

			os.system("grep '\[avilib\] V:' /tmp/pycoder |  perl -e ' $line=<STDIN> ; $line =~ /V: (.+?) fps/;print $1' > /tmp/vcd_fps")

			#fps=fpss)

			#fpss=int(fps)

			acode=open(os.path.join('/tmp/vcd_height'),'r')
			vcd_height=acode.read()
			acode.close()

			bcode=open(('/tmp/vcd_width'),'r')
			vcd_width=bcode.read()
			bcode.close()

			ccode=open(('/tmp/vcd_fps'),'r')
			fps=ccode.read()
			ccode.close()


			alturab =  height_out * 4 / 3

			altura = int(vcd_height) / (int(vcd_width) / alturab)

			altura=(int(altura))+int(altura)%2

			borde = (int(height_out)-altura)/2

			if self.transcode["border"]==self.lang.convertl11c:
				borde=borde
				aspect=2
			elif self.transcode["border"]==self.lang.convertl11b:
				borde=0
				altura=int(height_out)
				aspect=3
			elif self.transcode["border"]==self.lang.convertl11a:
				borde=0
				altura=int(height_out)
				aspect=2

			os.system("grep '\[avilib\] V:' /tmp/pycoder | perl -e ' $line=<STDIN> ; $line=~ /frames=(\d+)/  ;  print $1'>/tmp/frames")

			dcode=open(('/tmp/frames'),'r')
			vcd_frames=dcode.read()
			dcode.close()

			#fps
			#fpss=abs(fpss)+1
			#print int(round(fps))+1

			fps = fps.split( '.')[0]

			#fps=int(fps)+1

			#print fps
			#print vcd_frames
			totaltime=(int(vcd_frames)/int(fps))
			totalminuts=totaltime/60

			if self.transcode["cdsize"]=="700":
				cds=int(795)

			elif self.transcode["cdsize"]=="650":
				cds=int(735)

			elif self.transcode["cdsize"]=="800":
				cds=int(895)

			elif self.transcode["cdsize"]=="900":
				cds=int(995)

			#print self.transcode["cdsize"]
			#print self.transcode["audio"]
			#print totaltime

	#Calculamos el bitrate para el cvcd. Seg�n su tama�o, audiorate, etc, calculamos el maximo bitrate que usaremos

			#print fps
			#print cds
			#print totaltime
			#print self.transcode["audio"]

			videor=((cds*1024) - (int(self.transcode["audio"])/8*totaltime))*8/totaltime


#C�lculo aproximativo del maxr. Al calcular los fps a la baja para bajar el totaltime total y reducir el bitrate y asi reducir el tama�o que ocupara la pelicula,
#podemos intentat ajustar el c�lculo con un redondeo al alza.

			#maxr=videor*105/100

			maxr=videor

			#print videor
			#print maxr

			dconvert=wxMessageDialog(self,self.lang.codel22, self.lang.codel33, wxOK|wxICON_EXCLAMATION)
			dconvert.ShowModal()
			dconvert.Destroy()

			wxEXEC_SYNC=1

			wxExecute("""transcode -i %s -V -x mplayer -y mpeg2enc,mp2enc -F 2 ,-q 5 -Z %sx%s -Y -%s,0,-%s,0 --export_asr %s -E 44100 -w %s -b %s -o film""" % (self.pelicula,width_out,altura,borde,borde,aspect,maxr,self.transcode["audio"]),wxEXEC_SYNC)


	#Como solo creamos un cd, no necesitamos el calculo de tiempo para tcmplex


			wxExecute('tcmplex -i film.m1v -p film.mpa -o film-cd.mpg -m 1 -F maxFileSize=%s' % (cds),wxEXEC_SYNC)


		cconvert=wxMessageDialog(self,self.lang.codel2, self.lang.codel3, wxOK|wxICON_EXCLAMATION)
		cconvert.ShowModal()
		cconvert.Destroy()


####
#SAMPLE
#####
	def SampleMenu(self,e):

		menu0=wxMenu()
		menu0.Append(ID_SAMPLE, self.lang.convertl2, self.lang.convertl3)
		menu0.Append(ID_HOMEC,self.lang.convertl4,self.lang.convertl5)

		BarraX=wxMenuBar()
		BarraX.Append(menu0,self.lang.convertl6)
		self.SetMenuBar(BarraX)

		self.vmode.Destroy()
		self.vmode2.Destroy()

		#self.control.Destroy()

		self.convert0=wxStaticText(self,-1,self.lang.convertl12,wxPoint(40,30),wxSize(300,30))

		lista=['pal','ntsc']
		self.convert1 = wxStaticText(self, -1, self.lang.convertl7, wxPoint(20,70), wxSize(275, 50))
		self.convert2 = wxChoice(self, 40, (80, 100), choices = lista)

		EVT_CHOICE(self,40,self.ConvertVideoMode)


		lista2=['128','224']
		self.convert3 = wxStaticText(self,-1,self.lang.convertl8,wxPoint(20,150), wxSize(275,50))
		self.convert4 = wxChoice(self,41,(80,170),choices = lista2)


		EVT_CHOICE(self, 41, self.ConvertAudioMode)

		lista3=['5 min','10 min','20 min','30 min','60 min']

		self.convert5 = wxStaticText(self,-1,self.lang.convertlsample1,wxPoint(260,70), wxSize(275,50))
		self.convert6 = wxChoice(self,62,(300,100),choices = lista3)

		EVT_CHOICE(self,62,self.SampleTime)

		lista4=[self.lang.convertl11a,self.lang.convertl11b,self.lang.convertl11c]
		self.convert7 = wxStaticText(self, -1, self.lang.convertl11,wxPoint(260,150),wxSize(275,50))
		self.convert8=wxChoice(self,43,(300,170),choices = lista4)

		EVT_CHOICE(self,43,self.ConvertBorder)


	def Sample(self,e):

		self.convert0.Destroy()
		self.convert3.Destroy()
		self.convert1.Destroy()
		self.convert2.Destroy()
		self.convert4.Destroy()
		self.convert5.Destroy()
		self.convert6.Destroy()
		self.convert7.Destroy()
		self.convert8.Destroy()

		menu1=wxMenu()
		menu1.Append(ID_CONVERT,self.lang.samplel1,self.lang.samplel2)
		menu1.Append(ID_HOME,self.lang.convertl4,self.lang.convertl5)

#Creamos los menus llamando a lo definido anteriormente.

		Barra=wxMenuBar()
		Barra.Append(menu1,self.lang.barral1)

		self.SetMenuBar(Barra)

		self.vmode=wxStaticText(self, -1, self.lang.codel1, (0,0),style=wxALIGN_CENTER)
		#self.vmode.Destroy()
		self.vmode2=wxStaticText(self,-1," ",(20,20),style=wxALIGN_CENTER)

		if self.transcode["sampletime"]=="5 min":
			sampletime=5*60

		elif self.transcode["sampletime"]=="10 min":
			sampletime=10*60

		elif self.transcode["sampletime"]=="20 min":
			sampletime=20*60

		elif self.transcode["sampletime"]=="30 min":
			sampletime=30*60

		elif self.transcode["sampletime"]=="60 min":
			sampletime=60*60


###
#VCD-SAMPLE
###
		if self.transcode["vmode"]=="vcd":

			if self.transcode["video"] == "pal":

				fps=25
				width="352x288"
				width_out=352
				height_out=288.0
			#avifix -i self.pelicula -f 25
			#os.system("avifix -i"+" "+ self.pelicula+ " "+ "-f 25")

			else:
				#ntsc
				fps=29
				width="352x240"
				width_out=352
				height_out=240.0
			#avifix -i self.pelicula -f 29
			#os.system("avifix -i"+" "+self.pelicula+ " "+ "-f 29")


#Sacamos los valores originales de la pelicula, volcandolos a un tmp.
#El calculo de los bordes est� sacado de los scripts de paranouei en http://dvdripping-guid.berlios.de/Divx-to-VCD.html
#El calculo de bordes automatico resulta inestable. Con resoluciones en origen muy altas da como resultado imagenes mezcladas.
#Damos opcion de no usarlo

			os.system("grep '\[avilib\] V:' /tmp/pycoder | perl -e ' $line=<STDIN> ; $line =~ /width=(\d+)/; print $1' > /tmp/vcd_width")

			os.system("grep '\[avilib\] V:' /tmp/pycoder | perl -e ' $line=<STDIN> ; $line =~ /height=(\d+)/ ; print $1' > /tmp/vcd_height")


#Abrimos los archivos creados anteriormente con los valores que necesitamos

			acode=open(os.path.join('/tmp/vcd_height'),'r')
			vcd_height=acode.read()
			acode.close()

			bcode=open(('/tmp/vcd_width'),'r')
			vcd_width=bcode.read()
			bcode.close()

#Realizamos el calculo de los bordes

			alturab =  height_out * 4 / 3

			altura = int(vcd_height) / (int(vcd_width) / alturab)


#Redondeo para asegurar que el numero sea par. Si es impar transcode dar� error

			altura=(int(altura))+int(altura)%2

			borde = (int(height_out)-altura)/2


#Eleccion de si queremos borde o no. A la larga habra que implementar una calculadora de bordes.


			#borde calculado

			if self.transcode["border"]==self.lang.convertl11c:
				borde=borde
				aspect=2

			#borde mediano sacado con aspect 3

			elif self.transcode["border"]==self.lang.convertl11b:
				borde=0
				altura=int(height_out)
				aspect=3

			#Sin borde
			elif self.transcode["border"]==self.lang.convertl11a:
				borde=0
				altura=int(height_out)
				aspect=2


#Sacamos el numero de frames de la pelicula para poder calcular su duraci�n en segundos

			os.system("grep '\[avilib\] V:' /tmp/pycoder | perl -e ' $line=<STDIN> ; $line=~ /frames=(\d+)/  ;  print $1'>/tmp/frames")

			dcode=open(('/tmp/frames'),'r')
			vcd_frames=dcode.read()
			dcode.close()

#Calculamos la duracion de la pelicula, primero en segundos y luego en minutos

			totaltime=(int(vcd_frames)/fps)
			totalminuts=totaltime/60

			dconvert=wxMessageDialog(self,self.lang.codel22, self.lang.codel33, wxOK|wxICON_EXCLAMATION)
			dconvert.ShowModal()
			dconvert.Destroy()

#Creamos la funcion que rompe la creacion del video (killtransc)

			def killtransc():
				os.system("killall mplayer")
				os.system("killall transcode")
				os.system("killall tcdecode")
#Importamos la clase timer del modulo threading que nos permite crear un timer

			from threading import Timer

#Creamos el timer de tantos segundos invocando a la funcion anteriormente creada

			temps = Timer(sampletime, killtransc)
			temps.start()

			wxEXEC_SYNC=1
			wxExecute('transcode -i %s -V -x mplayer -y mpeg2enc,mp2enc -F 1 -Z %sx%s -Y -%s,0,-%s,0 --export_asr %s -E 44100 -w 1150 -R 3 -b %s -o sample' % (self.pelicula,width_out,altura,borde,borde,aspect,self.transcode["audio"]),wxEXEC_SYNC)

#Creamos el sample y lo llamamos film-sample

			wxExecute('tcmplex -m 1 -i sample.m1v -p sample.mpa -o film-sample.mpg' ,wxEXEC_SYNC)


###
#SVCD-SAMPLE
###

		elif self.transcode["vmode"]=="svcd":

			calidad="7"
			maxvbr="4000000"

 			if self.transcode["video"] == "pal":

				fps=25
				width="480x576"
				width_out=480
				height_out=576.0
				os.system("echo quant_value = %s > /tmp/template" % (calidad))
				os.system("echo max_bitrate = %s >> /tmp/template" % (maxvbr))
				mpeg_options="s,3,/tmp/template"

			else:
				#ntsc
				fps=29
				width="480x480"
				width_out=480
				height_out=480.0
				os.system("echo quant_value = %s > /tmp/template" % (calidad))
				os.system("echo max_bitrate = %s >> /tmp/template" % (maxvbr))
				mpeg_options="s,2,/tmp/template"


			os.system("grep '\[avilib\] V:' /tmp/pycoder | perl -e ' $line=<STDIN> ; $line =~ /width=(\d+)/; print $1' > /tmp/vcd_width")

			os.system("grep '\[avilib\] V:' /tmp/pycoder | perl -e ' $line=<STDIN> ; $line =~ /height=(\d+)/ ; print $1' > /tmp/vcd_height")


			acode=open(os.path.join('/tmp/vcd_height'),'r')
			vcd_height=acode.read()
			acode.close()

			bcode=open(('/tmp/vcd_width'),'r')
			vcd_width=bcode.read()
			bcode.close()

			alturab =  height_out * 4 / 3

			altura = int(vcd_height) / (int(vcd_width) / alturab)


			altura=(int(altura))+int(altura)%2

			borde = (int(height_out)-altura)/2


			if self.transcode["border"]==self.lang.convertl11c:
				borde=borde
				aspect=2

			elif self.transcode["border"]==self.lang.convertl11b:
				borde=0
				altura=int(height_out)
				aspect=3

			elif self.transcode["border"]==self.lang.convertl11a:
				borde=0
				altura=int(height_out)
				aspect=2


			os.system("grep '\[avilib\] V:' /tmp/pycoder | perl -e ' $line=<STDIN> ; $line=~ /frames=(\d+)/  ;  print $1'>/tmp/frames")

			dcode=open(('/tmp/frames'),'r')
			vcd_frames=dcode.read()
			dcode.close()

			totaltime=(int(vcd_frames)/fps)
			totalminuts=totaltime/60

			dconvert=wxMessageDialog(self,self.lang.codel22, self.lang.codel33, wxOK|wxICON_EXCLAMATION)
			dconvert.ShowModal()
			dconvert.Destroy()


			def killtransc():
				os.system("killall mplayer")
				os.system("killall transcode")
				os.system("killall tcdecode")

			from threading import Timer

			temps = Timer(sampletime, killtransc)
			temps.start()

			wxEXEC_SYNC=1
			wxExecute('transcode -i %s -V -k -x mplayer -y mpeg -F %s -Z %sx%s -Y -%s,0,-%s,0  --export_asr %s -R 3 -E 44100 -b %s -o sample' % (self.pelicula,mpeg_options,width_out,altura,borde,borde,aspect,self.transcode["audio"]),wxEXEC_SYNC)

			wxExecute('tcmplex  -m s -i sample.m2v -p sample.mpa -o film-sample.mpg' ,wxEXEC_SYNC)


###
#CVCD-sample
###

		elif self.transcode["vmode"]=="cvcd":

			if self.transcode["video"] == "pal":

				#fps=
				width="352x288"
				width_out=352
				height_out=288.0
			else:
				#ntsc
				#fps=23.976
				width="352x240"
				width_out=352
				height_out=240.0


			os.system("grep '\[avilib\] V:' /tmp/pycoder | perl -e ' $line=<STDIN> ; $line =~ /width=(\d+)/; print $1' > /tmp/vcd_width")

			os.system("grep '\[avilib\] V:' /tmp/pycoder | perl -e ' $line=<STDIN> ; $line =~ /height=(\d+)/ ; print $1' > /tmp/vcd_height")

			os.system("grep '\[avilib\] V:' /tmp/pycoder |  perl -e ' $line=<STDIN> ; $line =~ /V: (.+?) fps/;print $1' > /tmp/vcd_fps")

			#fps=fpss)

			#fpss=int(fps)

			acode=open(os.path.join('/tmp/vcd_height'),'r')
			vcd_height=acode.read()
			acode.close()

			bcode=open(('/tmp/vcd_width'),'r')
			vcd_width=bcode.read()
			bcode.close()

			ccode=open(('/tmp/vcd_fps'),'r')
			fps=ccode.read()
			ccode.close()


			alturab =  height_out * 4 / 3

			altura = int(vcd_height) / (int(vcd_width) / alturab)

			altura=(int(altura))+int(altura)%2

			borde = (int(height_out)-altura)/2

			if self.transcode["border"]==self.lang.convertl11c:
				borde=borde
				aspect=2
			elif self.transcode["border"]==self.lang.convertl11b:
				borde=0
				altura=int(height_out)
				aspect=3
			elif self.transcode["border"]==self.lang.convertl11a:
				borde=0
				altura=int(height_out)
				aspect=2

			os.system("grep '\[avilib\] V:' /tmp/pycoder | perl -e ' $line=<STDIN> ; $line=~ /frames=(\d+)/  ;  print $1'>/tmp/frames")

			dcode=open(('/tmp/frames'),'r')
			vcd_frames=dcode.read()
			dcode.close()

			#fps
			#fpss=abs(fpss)+1
			#print int(round(fps))+1

			fps = fps.split( '.')[0]

			#print fps
			#fps=int(fps)+1

			#print fps
			#print vcd_frames
			totaltime=(int(vcd_frames)/int(fps))
			totalminuts=totaltime/60

			if self.transcode["cdsize"]=="700":
				cds=int(795)

			elif self.transcode["cdsize"]=="650":
				cds=int(735)

			elif self.transcode["cdsize"]=="800":
				cds=int(895)

			elif self.transcode["cdsize"]=="900":
				cds=int(995)

		#	print self.transcode["cdsize"]
		#	print self.transcode["audio"]
		#	print totaltime

	#Calculamos el bitrate para el cvcd. Seg�n su tama�o, audiorate, etc, calculamos el maximo bitrate que usaremos

		#	print fps
		#	print cds
		#	print totaltime
		#	print self.transcode["audio"]

			videor=((cds*1024) - (int(self.transcode["audio"])/8*totaltime))*8/totaltime


#C�lculo aproximativo del maxr. Al calcular los fps a la baja para bajar el totaltime total y reducir el bitrate y asi reducir el tama�o que ocupara la pelicula,
#podemos intentat ajustar el c�lculo con un redondeo al alza.

			#maxr=videor*105/100

			maxr=videor

		#	print videor
		#	print maxr

			dconvert=wxMessageDialog(self,self.lang.codel22, self.lang.codel33, wxOK|wxICON_EXCLAMATION)
			dconvert.ShowModal()
			dconvert.Destroy()

			def killtransc():
				os.system("killall mplayer")
				os.system("killall transcode")
				os.system("killall tcdecode")

			from threading import Timer

			temps = Timer(sampletime, killtransc)
			temps.start()

			wxEXEC_SYNC=1

			wxExecute("""transcode -i %s -V -x mplayer -y mpeg2enc,mp2enc -F 2 ,-q 5 -Z %sx%s -Y -%s,0,-%s,0 --export_asr %s -E 44100 -w %s -b %s -o sample""" % (self.pelicula,width_out,altura,borde,borde,aspect,maxr,self.transcode["audio"]),wxEXEC_SYNC)


	#Como solo creamos un cd, no necesitamos el calculo de tiempo para tcmplex


			wxExecute('tcmplex -i sample.m1v -p sample.mpa -o film-sample.mpg -m 1 -F maxFileSize=%s' % (cds),wxEXEC_SYNC)


		cconvert=wxMessageDialog(self,self.lang.codel2, self.lang.codel3, wxOK|wxICON_EXCLAMATION)
		cconvert.ShowModal()
		cconvert.Destroy()



	def Grabar(self,e):


		#self.transcode = {'vmode':'vcd', 'video': 'pal','audio' : '128','cdsize' : '650','border': self.lang.yesno2}

		self.dirname=wxGetHomeDir()

		grabara=wxFileDialog(self,self.lang.grabarl1,self.dirname,"","*.mpg",wxOPEN)

		if grabara.ShowModal() == wxID_OK:
			self.filename=grabara.GetFilename()
			self.dirname=grabara.GetDirectory()
			archivocue=os.path.join(self.dirname,self.filename)

			self.control.Destroy()

			grabare = wxDialog(frame, -1, self.lang.grabarl9, size=wxSize(295, 115), style=wxCAPTION)

			texto = wxStaticText(grabare, -1, self.lang.grabarl1a ,wxPoint(5,10))

			videolista = ['vcd', 'svcd', 'cvcd']

			vmode2=wxRadioBox(grabare, 41, self.lang.grabarl1a, wxPoint(27,20),wxSize(250,50),videolista, 3,wxRA_SPECIFY_COLS | wxNO_BORDER)
			EVT_RADIOBOX(grabare, 41, self.VideoMode)

			boton = wxButton(grabare, wxID_OK, " OK ",wxPoint(100,80))
			boton.SetDefault()

			grabare.ShowModal()


			self.control=wxStaticText(self, -1, self.lang.grabarl2, (0,0),style=wxALIGN_LEFT)

			grabarc=wxMessageDialog(self,self.lang.grabarl11, self.lang.grabarl8, wxOK|wxICON_EXCLAMATION)
			grabarc.ShowModal()
			grabarc.Destroy()

			wxEXEC_SYNC=1

			if self.transcode["vmode"]=="vcd":

				wxExecute("vcdimager -t vcd2 -c film.cue -b film.bin %s" % (archivocue),wxEXEC_SYNC)

			elif self.transcode["vmode"]=="svcd":

				wxExecute("vcdimager -t svcd -c film.cue -b film.bin %s" % (archivocue),wxEXEC_SYNC)

			elif self.transcode["vmode"]=="cvcd":

				wxExecute("vcdimager -t vcd2 -c film.cue -b film.bin %s" % (archivocue),wxEXEC_SYNC)

			self.control.Destroy()
			self.control=wxStaticText(self, -1, self.lang.grabarl3, (0,0),style=wxALIGN_LEFT)

			grabarb=wxMessageDialog(self,self.lang.grabarl4, self.lang.grabarl5, wxOK|wxICON_EXCLAMATION)
			grabarb.ShowModal()
			grabarb.Destroy()

			self.control.Destroy()
			self.control=wxStaticText(self, -1, self.lang.grabarl6, (0,0),style=wxALIGN_LEFT)

			wxEXEC_SYNC=1
			wxExecute("cdrdao write --device %s --driver generic-mmc film.cue" % (self.config["cdrdaodev"]),wxEXEC_SYNC)

			grabard=wxMessageDialog(self,self.lang.grabarl7, self.lang.grabarl8, wxOK|wxICON_EXCLAMATION)
			grabard.ShowModal()
			grabard.Destroy()

		grabara.Destroy()


	def Vcd(self,e):


		vcd0=wxMessageDialog(self,self.lang.vcdl1, self.lang.vcdl2, wxOK|wxICON_EXCLAMATION)
		vcd0.ShowModal()
		vcd0.Destroy()

		os.system("mplayer -vo"+" "+ self.config["vo"] + " "+ "-vcd 2")

		self.control.Destroy()
		self.control=wxStaticText(self,-1,self.lang.vcdl3, (0,0),style=wxALIGN_LEFT)

	def Ver(self,e):

		self.dirname=wxGetHomeDir()

		aver=wxFileDialog(self,self.lang.verl1,self.dirname,"","*.*",wxOPEN)

		if aver.ShowModal() == wxID_OK:
			self.filename=aver.GetFilename()
			self.dirname=aver.GetDirectory()


			self.control.Destroy()
			self.control=wxStaticText(self,-1,self.lang.verl2, (0,0),style=wxALIGN_LEFT)

					
			os.system("mplayer -vo"+" "+ self.config["vo"] +" "+`os.path.join(self.dirname,self.filename)`)

			self.control.Destroy()
			self.control=wxStaticText(self,-1,self.lang.verl3, (0,0),style=wxALIGN_LEFT)


		aver.Destroy()	


	def Ayuda(self,e):


		#ver1="Ejecutando el navegador para abrir la ayuda..."
		#self.control.SetValue(lang.ayudal1)	


		wxExecute("%s http://acidnet.dyndns.org/vcd" % (self.config["navegador"]))


		#wxExecute( navegador "http://acidnet.dyndns.org",wxEXEC_SYNC)
		#ayuda1=wxMessageDialog(self,"Aqui va la ayuda\n" " ","Ayuda...",wxOK|wxICON_INFORMATION)
		#ayuda1.ShowModal()
		#ayuda1.Destroy()


	def Prefer(self,e):


		menu0=wxMenu()
		menu0.Append(ID_HOMEP,self.lang.preferl3,self.lang.preferl4)

		BarraX=wxMenuBar()
		BarraX.Append(menu0,self.lang.preferl5)
		self.SetMenuBar(BarraX)

		self.control.Destroy()

		preferlista1 = ['x11', 'xv', 'vesa', 'opengl']
		preferlista2 = ['mozilla', 'konqueror', 'galeon']


		self.pref1=wxRadioBox(self, 50, self.lang.preferl7,wxPoint(20,20), wxSize(200,80),preferlista1, 2, wxRA_SPECIFY_COLS)
		EVT_RADIOBOX(self, 50, self.PrefMplayer)
 
		self.pref2=wxRadioBox(self, 51, self.lang.preferl8, wxPoint(20,120),wxSize(250,50),preferlista2, 3, wxRA_SPECIFY_COLS)
		EVT_RADIOBOX(self, 51, self.PrefNavegador)

		preferlista3=['0,0,0','0,1,0','0,2,0','0,3,0','0,4,0','0,5,0','0,6,0','0,7,0']

		self.pref3 = wxStaticText(self,-1,self.lang.preferl9,wxPoint(270,20), wxSize(275,50))
		self.pref4 = wxChoice(self,52,(270,50),choices = preferlista3)

		EVT_CHOICE(self, 52, self.CdrdaoDev)


	def Homec(self,e):

		self.convert0.Destroy()		
		self.convert3.Destroy()
		self.convert1.Destroy()
		self.convert2.Destroy()
		self.convert4.Destroy()
		self.convert5.Destroy()
		self.convert6.Destroy()
		self.convert7.Destroy()
		self.convert8.Destroy()

		self.control=wxStaticText(self, -1, self.lang.textl2, (0,0),style=wxALIGN_CENTER)

		menu1=wxMenu()
		menu1.Append(ID_ABRIR,self.lang.menul1,self.lang.menul11)
		menu1.Append(ID_GRABAR,self.lang.menul3,self.lang.menul33) 
		menu1.AppendSeparator()
		menu1.Append(ID_SALIR,self.lang.menul4,self.lang.menul44)

		menu4=wxMenu()
		menu4.Append(ID_VER,self.lang.menul5,self.lang.menul55)
		menu4.Append(ID_VCD,self.lang.menul6,self.lang.menul66)

		menu5=wxMenu()
		submenumpg=wxMenu()
		submenuavi=wxMenu()
		submenumpg.Append(ID_MPGINFO,self.lang.menutools1)
		#submenumpg.Append(ID_MPGCORTAR,self.lang.menutools2)
		#submenumpg.Append(ID_MPGPEGAR,self.lang.menutools3)
		menu5.AppendMenu(ID_TOOLS,self.lang.menutools0,submenumpg,self.lang.menutools0a)
		submenuavi.Append(ID_AVIINFO,self.lang.menutools1)
		#submenuavi.Append(ID_AVICORTAR,self.lang.menutools2)
		#submenuavi.Append(ID_AVIPEGAR,self.lang.menutools3)
		menu5.AppendMenu(ID_TOOLS,self.lang.menutools4,submenuavi,self.lang.menutools4a)

		menu2=wxMenu()
		menu2.Append(ID_PREFS,self.lang.menul7,self.lang.menul77)

		submenulang = wxMenu()
		submenulang.Append(ID_LANGES,"Espa�ol")
		submenulang.Append(ID_LANGENG,"English")

		menu2.AppendMenu(ID_LANG,self.lang.menul7a,submenulang,self.lang.menul77a)

		menu3=wxMenu()
		menu3.Append(ID_AYUDA, self.lang.menul8, self.lang.menul88)
		menu3.Append(ID_ACERCA,self.lang.menul9, self.lang.menul99)	

#Creamos los menus llamando a lo definido anteriormente.

		Barra=wxMenuBar()
		Barra.Append(menu1,self.lang.barral1)
		Barra.Append(menu5,self.lang.barral5)
		Barra.Append(menu4,self.lang.barral2)
		Barra.Append(menu2,self.lang.barral3)
		Barra.Append(menu3,self.lang.barral4)
		
		self.SetMenuBar(Barra)


	def Homep(self,e):

		self.pref1.Destroy()
		self.pref2.Destroy()
		self.pref3.Destroy()		
		self.pref4.Destroy()

		self.control=wxStaticText(self, -1, self.lang.textl2, (0,0),style=wxALIGN_CENTER)

		menu1=wxMenu()
		menu1.Append(ID_ABRIR,self.lang.menul1,self.lang.menul11)
		menu1.Append(ID_GRABAR,self.lang.menul3,self.lang.menul33) 
		menu1.AppendSeparator()
		menu1.Append(ID_SALIR,self.lang.menul4,self.lang.menul44)

		menu4=wxMenu()
		menu4.Append(ID_VER,self.lang.menul5,self.lang.menul55)
		menu4.Append(ID_VCD,self.lang.menul6,self.lang.menul66)

		menu5=wxMenu()
		submenumpg=wxMenu()
		submenuavi=wxMenu()
		submenumpg.Append(ID_MPGINFO,self.lang.menutools1)
		#submenumpg.Append(ID_MPGCORTAR,self.lang.menutools2)
		#submenumpg.Append(ID_MPGPEGAR,self.lang.menutools3)
		menu5.AppendMenu(ID_TOOLS,self.lang.menutools0,submenumpg,self.lang.menutools0a)
		submenuavi.Append(ID_AVIINFO,self.lang.menutools1)
		#submenuavi.Append(ID_AVICORTAR,self.lang.menutools2)
		#submenuavi.Append(ID_AVIPEGAR,self.lang.menutools3)
		menu5.AppendMenu(ID_TOOLS,self.lang.menutools4,submenuavi,self.lang.menutools4a)

		menu2=wxMenu()
		menu2.Append(ID_PREFS,self.lang.menul7,self.lang.menul77)

		submenulang = wxMenu()
		submenulang.Append(ID_LANGES,"Espa�ol")
		submenulang.Append(ID_LANGENG,"English")

		menu2.AppendMenu(ID_LANG,self.lang.menul7a,submenulang,self.lang.menul77a)

		menu3=wxMenu()
		menu3.Append(ID_AYUDA, self.lang.menul8, self.lang.menul88)
		menu3.Append(ID_ACERCA,self.lang.menul9, self.lang.menul99)

#Creamos los menus llamando a lo definido anteriormente.

		Barra=wxMenuBar()
		Barra.Append(menu1,self.lang.barral1)
		Barra.Append(menu5,self.lang.barral5)
		Barra.Append(menu4,self.lang.barral2)
		Barra.Append(menu2,self.lang.barral3)
		Barra.Append(menu3,self.lang.barral4)

		self.SetMenuBar(Barra)


	def Home(self,e):

		#self.control.Destroy()

		if self.control:
			self.control.Destroy()

		if self.vmode:
			self.vmode.Destroy()
			self.vmode2.Destroy()

		self.control=wxStaticText(self, -1, self.lang.textl2, (0,0),style=wxALIGN_CENTER)

		menu1=wxMenu()
		menu1.Append(ID_ABRIR,self.lang.menul1,self.lang.menul11)
		menu1.Append(ID_GRABAR,self.lang.menul3,self.lang.menul33)
		menu1.AppendSeparator()
		menu1.Append(ID_SALIR,self.lang.menul4,self.lang.menul44)

		menu4=wxMenu()
		menu4.Append(ID_VER,self.lang.menul5,self.lang.menul55)
		menu4.Append(ID_VCD,self.lang.menul6,self.lang.menul66)

		menu5=wxMenu()
		submenumpg=wxMenu()
		submenuavi=wxMenu()
		submenumpg.Append(ID_MPGINFO,self.lang.menutools1)
		#submenumpg.Append(ID_MPGCORTAR,self.lang.menutools2)
		#submenumpg.Append(ID_MPGPEGAR,self.lang.menutools3)
		menu5.AppendMenu(ID_TOOLS,self.lang.menutools0,submenumpg,self.lang.menutools0a)
		submenuavi.Append(ID_AVIINFO,self.lang.menutools1)
		#submenuavi.Append(ID_AVICORTAR,self.lang.menutools2)
		#submenuavi.Append(ID_AVIPEGAR,self.lang.menutools3)
		menu5.AppendMenu(ID_TOOLS,self.lang.menutools4,submenuavi,self.lang.menutools4a)

		menu2=wxMenu()
		menu2.Append(ID_PREFS,self.lang.menul7,self.lang.menul77)

		submenulang = wxMenu()
		submenulang.Append(ID_LANGES,"Espa�ol")
		submenulang.Append(ID_LANGENG,"English")

		menu2.AppendMenu(ID_LANG,self.lang.menul7a,submenulang,self.lang.menul77a)

		menu3=wxMenu()
		menu3.Append(ID_AYUDA, self.lang.menul8, self.lang.menul88)
		menu3.Append(ID_ACERCA,self.lang.menul9, self.lang.menul99)

#Creamos los menus llamando a lo definido anteriormente.

		Barra=wxMenuBar()
		Barra.Append(menu1,self.lang.barral1)
		Barra.Append(menu5,self.lang.barral5)
		Barra.Append(menu4,self.lang.barral2)
		Barra.Append(menu2,self.lang.barral3)
		Barra.Append(menu3,self.lang.barral4)

		self.SetMenuBar(Barra)

	
	def Langes(self,e):

		self.config["lang"]="lang_es"

		self.config = {'navegador': self.config["navegador"], 'vo' : self.config["vo"],'cdrdaodev': self.config["cdrdaodev"],'lang': self.config["lang"]}

		directorio=wxGetHomeDir()
		
		archivo=".pycoder"

		todo=os.path.join(directorio,archivo)

		k=open(todo,"w")
		pickle.dump(self.config, k)

		langesa=wxMessageDialog(self,"El idioma por defecto ha sido cambiado a espa�ol. Reinicia la aplicaci�n para que los cambios tengan efecto", "Cambio de idioma...",wxOK|wxICON_INFORMATION)
		langesa.ShowModal()
		langesa.Destroy()


	def Langeng(self,e):

		self.config["lang"]="lang_eng"

		self.config = {'navegador': self.config["navegador"], 'vo' : self.config["vo"],'cdrdaodev': self.config["cdrdaodev"],'lang': self.config["lang"]}

		directorio=wxGetHomeDir()

		archivo=".pycoder"
		
		todo=os.path.join(directorio,archivo)

		k=open(todo,"w")
		pickle.dump(self.config, k)
	
		langenga=wxMessageDialog(self,"The default language has been changed to english. Please, restart this app to the changes has effect", "Languange change...",wxOK|wxICON_INFORMATION)
		langenga.ShowModal()
		langenga.Destroy()


#Eventos de Convert

	def ConvertVideoMode(self,event):

	#	class TestTransientPopup(wxPopupTransientWindow):

	#		def __init__(self, parent, style, log):
	#			wxPopupTransientWindow.__init__(self, parent, style)

	#			panel = wxPanel(self, -1)
	#			panel.SetBackgroundColour("#FFB6C1")
	#			st = wxStaticText(panel, -1,
	#			"wxPopupTransientWindow is a\n"
	#			"wxPopupWindow which disappears\n"
	#			"automatically when the user\n"
	#			"clicks the mouse outside it or if it\n"
	#			"loses focus in any other way." ,  pos=(10,10))
	#			sz = st.GetBestSize()
	#			panel.SetSize( (sz.width+20, sz.height+20) )
	#			self.SetSize(panel.GetSize())



		self.transcode["video"]='%s' % event.GetString()


	def ConvertAudioMode(self,event):



		self.transcode["audio"]='%s' % event.GetString()


	def ConvertCdSize(self,event):


		self.transcode["cdsize"]='%s' % event.GetString()


	def ConvertBorder(self,event):


		self.transcode["border"]='%s' % event.GetString()


	def SampleTime(self,event):

		self.transcode["sampletime"]='%s' % event.GetString()


	def VideoMode(self,event):

		self.transcode["vmode"]='%s' % event.GetString()


		if event.GetString()=="vcd":

			self.vmode2.Destroy()
			self.vmode2=wxStaticText(self,-1,self.lang.explicavcd,(50,100),style=wxALIGN_LEFT)


		elif event.GetString()=="svcd":

			self.vmode2.Destroy()
			self.vmode2=wxStaticText(self,-1,self.lang.explicasvcd,(50,100),style=wxALIGN_LEFT)


		elif event.GetString()=="cvcd":

			self.vmode2.Destroy()
			self.vmode2=wxStaticText(self,-1,self.lang.explicacvcd,(50,100),style=wxALIGN_LEFT)


	def PrefNavegador(self, event):


		self.config["navegador"]='%s' % event.GetString()

		self.config = {'navegador': self.config["navegador"], 'vo' : self.config["vo"],'cdrdaodev': self.config["cdrdaodev"],'lang': self.config["lang"]}

		directorio=wxGetHomeDir()

		archivo=".pycoder"

		todo=os.path.join(directorio,archivo)


		k=open(todo,"w")
		pickle.dump(self.config, k)


	def PrefMplayer(self, event):


		self.config["vo"]='%s' % event.GetString()

		self.config = {'navegador': self.config["navegador"], 'vo' : self.config["vo"],'cdrdaodev': self.config["cdrdaodev"],'lang':self.config["lang"]}

		directorio=wxGetHomeDir()

		archivo=".pycoder"

		todo=os.path.join(directorio,archivo)

		k=open(todo,"w")
		pickle.dump(self.config, k)


	def CdrdaoDev(self, event):
        	
		self.config["cdrdaodev"]='%s' % event.GetString()
	
		self.config = {'navegador': self.config["navegador"], 'vo' : self.config["vo"],'cdrdaodev': self.config["cdrdaodev"],'lang': self.config["lang"]}
	
		directorio=wxGetHomeDir()
		
		archivo=".pycoder"

		todo=os.path.join(directorio,archivo)

		k=open(todo,"w")
		pickle.dump(self.config, k) 

###
#MPGTOOLS
###

	def MpgInfo(self,e):

		self.dirname=wxGetHomeDir()
		
		ampginfo=wxFileDialog(self,self.lang.abrirl1,self.dirname,"","*.mpg",wxOPEN)

		if ampginfo.ShowModal() == wxID_OK:
		
			self.filename=ampginfo.GetFilename()
			self.dirname=ampginfo.GetDirectory()
			mpginfopeli=os.path.join(self.dirname,self.filename)
			os.system('mpgtx -i'+" "+`mpginfopeli` +" >/tmp/pycoder")

			bmpginfo=open(os.path.join('/tmp/pycoder'),'r')
			cmpginfo=bmpginfo.read()
			j=wxMessageDialog(self,cmpginfo,self.lang.textl3,wxOK)
			j.Show()
			bmpginfo.close()


	def MpgCortar(self,e):


		self.dirname=wxGetHomeDir()

		grabara=wxFileDialog(self,self.lang.grabarl1,self.dirname,"","*.mpg",wxOPEN)

		if grabara.ShowModal() == wxID_OK:
			self.filename=grabara.GetFilename()
			self.dirname=grabara.GetDirectory()
			archivompg=os.path.join(self.dirname,self.filename)

			self.control.Destroy()


###
#AVITOOLS
###	

	def AviInfo(self,e):
	
		self.dirname=wxGetHomeDir()

		aaviinfo=wxFileDialog(self,self.lang.abrirl1,self.dirname,"","*.avi",wxOPEN)

		if aaviinfo.ShowModal() == wxID_OK:
		
			self.filename=aaviinfo.GetFilename()
			self.dirname=aaviinfo.GetDirectory()
			aviinfopelicula=os.path.join(self.dirname,self.filename)
			os.system('tcprobe -i'+" "+`aviinfopelicula` +" >/tmp/pycoder")
			
			baviinfo=open(os.path.join('/tmp/pycoder'),'r')
			caviinfo=baviinfo.read()
			j=wxMessageDialog(self,caviinfo,self.lang.textl3,wxOK)
			j.Show()
			baviinfo.close()


app=wxPySimpleApp()
frame = Ventana(None,-1,"Vcd PyCoder 0.1.1")
app.MainLoop()
